trigger AccountTrigger on Account (before insert,before update) {
   
 AccountTriggerHandler instance =  AccountTriggerHandler.getInstance();
 
 if(Trigger.isBefore){
    if(Trigger.isInsert){
        instance.beforeInsert(Trigger.New);
    } 
    if(Trigger.isUpdate){
        instance.beforeUpdate(Trigger.New);
    } 
   } 
}