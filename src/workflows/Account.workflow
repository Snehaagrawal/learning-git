<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Test</fullName>
        <description>Test</description>
        <protected>false</protected>
        <recipients>
            <recipient>pavithra@lib.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>sachin.chincholi@63demo.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/LeadsWebtoLeademailresponseSAMPLE</template>
    </alerts>
    <fieldUpdates>
        <fullName>Update_field</fullName>
        <field>Rating</field>
        <literalValue>Warm</literalValue>
        <name>Update field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
